Rails.application.routes.draw do
  # http://guides.rubyonrails.org/routing.html
  resources :articulos do
    resources :comentarios
  end

  root 'bienvenido#index'
  get 'bienvenido/index'
end
