# Proyecto de Construccion con Ruby on Rails

## Descripcion
Construccion de un proyecto entero con todo el CRUD y las practicas mencionadas

### Creador
Gabriel Vargas Monroy

### Como Ejecutar este Proyecto

1. clonar este repositorio
  ```sh
  → git clone https://gitlab.com/vmgabriel/ruby-blog-doc.git
  ```
2. Construir el Dockerfile
```sh
  → docker-compose build
  ```
3. Con ello, si no hay error de ningun tipo se puede ejecutar el servidor
```sh
  → docker-compose up
  ```

4. Ya se puede abrir en el navegador
[127.0.0.1:3000](http://127.0.0.1:3000 "Link a Pagina")
   
### Tecnologias
Las tecnologias en uso son las siguientes:

1. Ruby on Rails
![Ruby on Rails](img/rubyOnRails.png "Logo Ruby On Rails")

2. sqlite 3
![Sqlite 3](img/sqLite3.png "Logo Sqlite 3")

3. Docker
![Docker](img/docker.jpg "Logo Docker")

### Licencia
GNU-GPL 3.0
